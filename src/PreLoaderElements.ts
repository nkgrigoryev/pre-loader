export default class PreLoaderElement {
    private static readonly _PRE_LOADER_ID: string = 'pre-loader';

    private static _element: HTMLElement;

    public static get element() {
        if (PreLoaderElement._element) return PreLoaderElement._element;
        PreLoaderElement._element = document.getElementById(PreLoaderElement._PRE_LOADER_ID);
        return PreLoaderElement._element;
    }
}