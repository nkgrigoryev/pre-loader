const path = require('path');

module.exports = {
    entry: './src/dist.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'pre-loader.min.js'
    },
    resolve: {
        extensions: [
            '.ts',
            '.tsx',
            '.js'
        ]
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }, {
                test: /\.ts?$/i,
                loader: 'ts-loader',
                options: {
                    allowTsInNodeModules: true
                }
            }, {
                test: /\.(gif|png|jpeg|jpg|svg)$/i,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 500000, // Convert images < 500kb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    }
                },
                ],
            }
        ]
    },
    mode: 'production'
};