import './preLoader.css';
import PreLoaderElement from "./PreLoaderElements";

export default class PreLoader {
    private static readonly _CLASS: string = 'PreLoader__hide';
    private static readonly _DELAY: number = 200;
    private static _hidden: boolean = false;

    public static hide() {
        if (PreLoader._hidden) return;
        PreLoader._hidden = true;
        PreLoaderElement.element.classList.add(PreLoader._CLASS);
        setTimeout(() => {
            PreLoaderElement.element.style.display = 'none';
        }, PreLoader._DELAY)
    }
}