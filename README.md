# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Build dist
```bash
yarn webpack --config webpack.config.dist.js
```

## Use
```html
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@casino/pre-loader</title>
    <link rel="shortcut icon" href="./demo/favicon.ico">
</head>
<style>
    html,
    body,
    #content,
    #pre-loader {
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
    }

    #pre-loader {
        position: fixed;
        background: #222;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    #pre-loader-animation {
        display: none;
    }
</style>
<body>
    <div id="content" style="background: #F66">4</div>
    <div id="pre-loader">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAADoCAMAAADSd1hKAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVxQTFRFIiIiIiIjIiMjIiQkIiUlIyUmIyYmIyYnIycnIycoIygoIygpIykpIykqJCoqJCorJCsrJCssJCwtKTIzMz4/PEZHRk9RV2RmXW1uRVdZLTo7JC4vZnR1eY6Qmba5vNvezfDz0fP2zvT3zfX4z/X5y+bpLDU2MTY3zvX50Pb40/b50vb4yvDzUFlaMzo7w+Ll1vf5yPT4xPP32Pf51Pb4yvT41vb4z/b5zPT3zfP3wubqx/P3vvL2vfL2wvP2ru/0uPH21fb51ff5O0JD2Pb5zPT4tvD1xfP3wPL2qe7zs+/11/b51/f6zPL1xerupe3zouzz1fX4QUpLnOvyn+zyyfL2q+/0iZ6huvH2mK6xp77Ab4WHKjY3u9bZyu7xJS0uvvD1uuHlxvH21vb5hJWXy+rtirK2vO7yt/D1tN7hpc3RJy8vbICBteHmxvL2o7i6vPH20vX5tNXYd4eJvJd2xgAACANJREFUeJzt3f1z0zYYB/C2dKWDQZMUCqy8lDgDhwRYRgJNg+0wWsdW1jrUIQQYdGxsY4O9sP3/d8uLXyRZcpy2w1L7fO+444dw1w/PI9mxJXVmBgKBQCAQCAQCgUCSZZaVtH+owwwTeNS8SZDyY6dRyouNIuZYkdyahMjnpv3TJ850SCY2bUKS7AfJsKbNmJQDMClr2pL4HJApCzWOeYKTOGraHl64TB6SqxWbymFOVDKt4krZ5UyoZFhFlR6cSVPFlLKcUzM51LRteBjOfTEpqnDSqJPvmPcipTRROeeZmUAVS5qgnGwlFyumlO7b6ZRsKy1N2zjMBCem+SyaBFRhpLFOCrlAhMIKLk3iJJEnhyG1TKpY0jhnyMSEZAIriyoeNM7pMRlIHBtQRZWSBeUxMdViGDaV37zpQ7lOgrnIDGZlFFWYkhIFZTvjlLiVXVRRpLyCek6/nD7pczqENSxqVCoCNM6JMSNI0upR46WpOhmNS7QtyTyFhbTypCLMR5wRGnViyNNBcG1A5UtTh3KcC6EzUI58X/jxtSEVlwrVvMwRio9PnIkbCaxn5UuFgkYLijl5TN/qVTWJNG3oBGegPEOGovKkaUNnOVDa6VfzDCselZSyS5pW7/Kc8+F8i5fTg50NQ1BjpelDWTPRoKBRZ4BcChJivaKGUq95BSkpBxo2buj0mUt0PGoo9eZeoUYpAxoWNOJkKD2qX9RAyiipIFDGCPWgEWcGD04NxmlcSVOBhnMRY4RSTo+ZyWRzuVzWj29lSdklTQ0aU9Bx4+LOMZNI1quqJyWalwf95FI2lCwo6aSZntWXYsN0PErx3hUKSo3QsKAjZ6ScARWX8kqa4mzEg0YLOnLymJ707FmieYOSpj9I2XMR1rmjmchvXMy5fO78yoWLl85/uRqREiWleveEOFB8zqUK6jmXL1+5em3tel4ZpJD/6urllRuYdFzSU2FJGdPRpx+kjBvd4K6I6FzMefPaUKioaqF4q1RUh38v3w6k5Cjl9K6A0KBzB9DxPHTnrqKWSl9Xvindq9bul5QHt0rqev0SJR2VVKBByoVSnRsW9NxGoVptPCxpJVUvGkqt+ahRK35br6+M/xso6Lh32RcYUaCRzh1BHm/U721uFU1dN1vWlmGbSGm3lPZ329tjadabjkbzLv8CIyZ0POWOCnp+Z6etNBqoUrMdq2M4jq6purnVaD9ZLwdSr6TiQ/Eheoro3N3yRr2tNlRV0/QWck2ji5xKzVKV+5tbSnmXhrKmXWGg9FyEQ3efrt99UtP1olKz9I6FeqaNLFsv6pVasVpWyo+jgzQyG4kIpeaibPaZUl9rK33dMDom6tmG2ze1Xr/ZK1aV9r0dRXk++C6zFBmkEkJXFOXW/arWN3Vk2obpukbTdc2WZqJ2ZXNN2bmTHd7bs6HzYkLxq8v4Kjr4dpbNXlaKhZpp9FB/8McxXNt1XYQM1GhWiwVlJTuCLskDXWBDByPwhVKqPOrZjtNxEOoaHcMwBtKeabarysvcEYEO59TvNwoVvaU7tu3adtd1u45r9HotTd9UX60OnzoMv4IfBejgAlO1kKm5Q2PH7RhNZHWR1lAfvB5OubJVlDNGh9C9C2s/KBbqaK4zmIu6drczuDvqt4r5izkpocxZd3zf/vLNj2Z/MDbNZsd0kG1bjT76Kf9yNYBKNeuyLy8j6OrbN8jSuo49uLS4JrKbGkLlZ3tjJx8q8nV0kYaOpctXflZtQ7f6TrOrDVr3Yf/pi/FjhtH9gix3Rvx73awvvfPLrxWnYTeRbTmOXXvX+m05FxZUlntd/reXoKR7l37P1wxD0/tNC6FCfuec76Sgon974XwfDUv6/sMffz60kIZaeuXd9fJtf4Ayh6jw0OhsFEqXn63ldaNfU1EpX/+QC50Z6Z4wsJ4ZhdDc+7+2163Gu78bH+vP93J4QWV7ZkQNUrqkuff/bOc3a9XC9VerlPOskE8B457rRh/U49IbG9sfSx8Lr2+SjSvqc91pn9RnQ+nu+vCJ7m3aST+pPynIk/rp3r1kcOnFgfMDwynmu5cJb9Ow18DB29HA+lZ5myWdEr5Ni3k/Gkj3/t3zX31nMvz3o+yrqADQiW+8M1kqGfKNN7+gaUOnXsNAWCVZw8B/t8+UJlmVQq6/EWVVCrd3p11nxHGKuM7oEFeOxS4zEgt6NNcCxqzuXGRIY1Z34k7BVncmXK9Lr2OVb71uTEmP2ArsKdbU85fUS7amnrFLgt4MwrbKs0sitqQRKo4NdvnIse8lyQ4f9k6m0/LsZDrg3rRgc5oku/Aio5SSTtht6G03FNs55f5RHlKS/aNxO4KDjc+LMRl/4qT4O4I/wR7vtDv3sHbtczZ4i7FLFpfOsaRH6xyGaU7W4CHlOFlj+rNScKJEZ6Uc5PSbBalOv0l+nhHjOCOpzjOafBLX/ORE/o2AziRnjk2pFPTMsQOdIsc8Rk5UZ2SYJjkXkH8w4JywzuNz0uMhSufEdv4fp7GK6eQdI3xgpnBO7sHQ+1KK7Jzhn4E9tVLwM7BjTvuO00Y/KjpzmDgqBeZ9YFYG5/H5zQMzx+Z3SQwzuy/rrGzMYYgfOgGW/Lw0zFFm6SQzSqYcJUJIkLR/5n3neCi9HAsklqMvhEAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAI5NDzH9LFnBDC+WNvAAAAAElFTkSuQmCC" alt="Pre-loader">
    </div>
</body>
<script src="./dist/pre-loader.min.js"></script>
<script src="./demo/bundle.js"></script>
```

Run after loading
```js
import PreLoader from "@casino/pre-loader";
 PreLoader.hide();
```