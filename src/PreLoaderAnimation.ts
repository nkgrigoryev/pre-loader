import gif from './img/pre-loader.gif';
import PreLoaderElement from "./PreLoaderElements";

export default class PreLoaderAnimation {
    public static readonly LOAD_EVENT: string = 'load';

    public static run() {
        let image = new Image();
        image.addEventListener(PreLoaderAnimation.LOAD_EVENT, () => {
            PreLoaderElement.element.innerHTML = `<img src="${gif}" alt="Pre-loader">`;
        });
        image.src = gif;
    }
}